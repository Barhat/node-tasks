let bodyParser = require("body-parser");
let express = require("express");
let app = express();

app.use(express.static("../client"));
reviewArchive = [];

let jsonParser = bodyParser.json();

app.get("/list", function(request, response) {
    let last = Math.min(3, reviewArchive.length);
    let lastReviews = [];
    for (let i = 0; i < last; i++) {
        let entry = {
            name: reviewArchive[i].name,
            text: reviewArchive[i].text
        }
        lastReviews.push(entry);
    }
    response.send(lastReviews);
});

app.post("/post", jsonParser, function(request, response) {
    let newEntry = {
        text: request.body.text,
        name: request.body.name
    }
    reviewArchive.unshift(newEntry);
    response.send();
});

app.listen(80);
