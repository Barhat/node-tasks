let express = require("express");
let fs = require("fs");
let app = express();

app.get("/index.html", function(request, response) {
    fs.readFile("../client/index.html", "utf8", function(err, contents) {
        response.send(contents);
    })
})

app.get("/random", function(request, response) {
    response.send({x: Math.random()});
})

app.listen(80);
