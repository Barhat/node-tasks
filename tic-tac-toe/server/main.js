let express = require("express");
let app = express();

app.use(express.static("../client"));

app.get("/evaluate", function(request, response) {
    let position = request.query.position;
    let ROWS = 3;
    let COLS = 3;
    let LENGTH = 3;
    function getCellPosition(y, x) {
        return position[COLS * y + x];
    }
    let direction = [[0, 1], [1, 0], [1, 1], [-1, 1]];
    function inside(y, x) {
        return (0 <= x && x < COLS && 0 <= y && y < ROWS);
    }
    function testDirection(y, x, dy, dx) {
        let value = getCellPosition(y, x);
        for (let i = 1; i < LENGTH; i++) {
            if (!inside(y + dy * i, x + dx * i) || getCellPosition(y + dy * i, x + dx * i) !== value) {
                return false;
            }
        }
        return true;
    }
    function getResult(char) {
        for (let i = 0; i < ROWS; i++) {
            for (let j = 0; j < COLS; j++) {
                let value = getCellPosition(i, j);
                if (value === char) {
                    for (let [dy, dx] of direction) {
                        if (testDirection(i, j, dy, dx)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    let xWin = getResult('X');
    let oWin = getResult('O');
    let result = 0;
    if (xWin) {
        result += 1;
    }
    if (oWin) {
        result += 2;
    }
    response.send({result});
})

app.listen(80);
